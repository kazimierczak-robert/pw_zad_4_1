#include <windows.h>
#include <Commctrl.h>
#include <fstream>
#include <omp.h>
using namespace std;
LPSTR NazwaKlasy = "Klasa Okna";
MSG Komunikat;
WNDPROC g_OldWndProc;
#pragma warning (disable:4996)

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK NewWndProc(HWND hwnd, UINT mesg, WPARAM wParam, LPARAM lParam);
HWND n, hText, n_silnia, n_silnia_wynik, hProgressBar, start, pause, stop, procent_wyk, procent, czas_wyk, czas_wyk_wynik, czas_poz, czas_poz_wynik;
LPSTR Bufor, Bufor2;
int zawartosc_bufora = 0;
volatile int ilosc_wyk_perm = 0;
fstream plik;
int *Table;
DWORD dlugosc, dwThreadId, dwThreadId2;
HANDLE ahThread, ahThread2;
volatile int stan = 0;
volatile HWND hwnd;
double pocz, znacznik_czasu, sum;
int silnia(int n)
{
	{
		if (n == 0 || n == 1) return 1;
		else return n*silnia(n - 1);
	}
}
void GetDesktopResolution(int& horizontal, int& vertical)
{
	RECT desktop;
	const HWND hDesktop = GetDesktopWindow();
	GetWindowRect(hDesktop, &desktop);
	horizontal = desktop.right;
	vertical = desktop.bottom;
}
void out(int *T, int N)
{
	for (int i = 0; i < N; i++) plik << T[i]; // 1234 bez rozdzielania...
	plik << endl; // nowa linia;
}
void permutacje(int *T, int ix, int N) // ix = 0..N-1
{
	if (ix < N - 1)
	{
		for (int i = ix; i < N; i++)
		{
			swap(T[ix], T[i]); // zamienia dwa elementy miejscami
			permutacje(T, ix + 1, N);
			swap(T[ix], T[i]); // przywracamy
		}
	}
	else
	{
		out(T, N); // drukujemy wszystkie elementy
		ilosc_wyk_perm += 1;
	}
}
DWORD WINAPI ThreadFunc_Permutacje(LPVOID lpdwParam) 
{
	sum = 0;
	pocz = omp_get_wtime();
	permutacje(Table, 0, zawartosc_bufora);
	stan = 0;
	delete Table;
	plik.close();
	GlobalFree(Bufor);
	MessageBox(NULL, "Zako�czono!", "OK", MB_ICONINFORMATION | MB_OK);
	return 0;
}
DWORD WINAPI ThreadFunc_ScreenControl(LPVOID lpdwParam) 
{
	SetDlgItemText(hwnd, 4, itoa(silnia(zawartosc_bufora), Bufor2, 10));
	while (ilosc_wyk_perm < silnia(zawartosc_bufora))
	{
		SendMessage(hProgressBar, PBM_SETPOS, (WPARAM)(ilosc_wyk_perm * 100 / silnia(zawartosc_bufora)), 0);
		SetDlgItemText(hwnd, 9, itoa(ilosc_wyk_perm * 100 / silnia(zawartosc_bufora), Bufor2, 10));
		znacznik_czasu = omp_get_wtime();
		SetDlgItemText(hwnd, 10, itoa(sum + (znacznik_czasu - pocz), Bufor2, 10));
		SetDlgItemText(hwnd, 12, itoa((100 - (int)(ilosc_wyk_perm * 100 / silnia(zawartosc_bufora)))*(sum + (znacznik_czasu - pocz)) / (int)(ilosc_wyk_perm * 100 / silnia(zawartosc_bufora)), Bufor2, 10));
		Sleep(1000);
	}
	SetDlgItemText(hwnd, 9, itoa(ilosc_wyk_perm * 100 / silnia(zawartosc_bufora), Bufor2, 10));
	SendMessage(hProgressBar, PBM_SETPOS, (WPARAM)(ilosc_wyk_perm * 100 / silnia(zawartosc_bufora)), 0);
	SetDlgItemText(hwnd, 10, itoa(sum + (znacznik_czasu - pocz), Bufor2, 10));
	SetDlgItemText(hwnd, 12, "0");
	return 0;
}
void start_program()
{
	dlugosc = GetWindowTextLength(hText);
	Bufor = (LPSTR)GlobalAlloc(GPTR, dlugosc + 1);
	GetWindowText(hText, Bufor, dlugosc + 1);
	zawartosc_bufora = atoi(Bufor);
	if (zawartosc_bufora < 1) MessageBox(NULL, "Podano za ma�y argument!", "ERROR", MB_ICONEXCLAMATION | MB_OK);
	else if (zawartosc_bufora > 10) MessageBox(NULL, "Podano za du�y argument!", "ERROR", MB_ICONEXCLAMATION | MB_OK);
	else
	{
		plik.open("permutacje.txt", ios::out);
		if (plik.good() == true)
		{
			Table = new int[zawartosc_bufora];
			for (int q = 0; q < zawartosc_bufora; q++)
			{
				Table[q] = q;
			}
			ilosc_wyk_perm = 0;
			stan = 1;
			ahThread = CreateThread(NULL, 0, ThreadFunc_Permutacje, 0, 0, &dwThreadId);
			ahThread2 = CreateThread(NULL, 0, ThreadFunc_ScreenControl, 0, 0, &dwThreadId2);
		}
		else
		{
			MessageBox(NULL, "Problem z plikiem!", "ERROR", MB_ICONEXCLAMATION | MB_OK);
		}
	}
}
void stop_program()
{
	if (stan == 2)
	{
		ResumeThread(ahThread);
		ResumeThread(ahThread2);
	}
	if (plik.good() == true) plik.close();
	delete Table;
	TerminateThread(ahThread, 0);
	TerminateThread(ahThread2, 0);
	GlobalFree(Bufor);
}
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = 0;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)5;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = NazwaKlasy;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if (!RegisterClassEx(&wc))
	{
		MessageBox(NULL, "B��d: Nie mo�na zarejestrowa� klasy okna!", "Error!", MB_ICONEXCLAMATION | MB_OK);
		return 1;
	}

	// TWORZENIE OKNA
	int horizontal = 0;
	int vertical = 0;
	GetDesktopResolution(horizontal, vertical);
	int width = 375;
	int height = 190;

	hwnd = CreateWindowEx(WS_EX_CLIENTEDGE, NazwaKlasy, "Permutacje", WS_OVERLAPPEDWINDOW,
		horizontal / 2 - width / 2, vertical / 2 - height / 2, width, height, NULL, NULL, hInstance, NULL);
	if (hwnd == NULL)
	{
		MessageBox(NULL, "B��d: Nie mo�na utworzy� okna!", "Error!", MB_ICONEXCLAMATION);
		return 1;
	}
	n = CreateWindowEx(0, "STATIC", "n", WS_CHILD | WS_VISIBLE | SS_CENTERIMAGE | BS_PUSHBUTTON, 20, 20, 25, 25, hwnd, (HMENU)1, hInstance, NULL);
	hText = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | ES_NUMBER, 37, 20, 50, 25, hwnd, (HMENU)2, hInstance, NULL);
	n_silnia = CreateWindowEx(0, "STATIC", "n!  =", WS_CHILD | WS_VISIBLE | SS_CENTERIMAGE | BS_PUSHBUTTON, 20, 50, 43, 25, hwnd, (HMENU)3, hInstance, NULL);
	n_silnia_wynik = CreateWindowEx(0, "STATIC", "0", WS_CHILD | WS_VISIBLE | SS_CENTERIMAGE | BS_PUSHBUTTON | SS_LEFT, 55, 50, 80, 25, hwnd, (HMENU)4, hInstance, NULL);
	start = CreateWindowEx(0, "BUTTON", "Start/Kontynuuj", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 140, 10, 145, 25, hwnd, (HMENU)5, hInstance, NULL);
	pause = CreateWindowEx(0, "BUTTON", "Pauza", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 140, 40, 70, 25, hwnd, (HMENU)6, hInstance, NULL);
	stop = CreateWindowEx(0, "BUTTON", "Stop", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 215, 40, 70, 25, hwnd, (HMENU)7, hInstance, NULL);
	hProgressBar = CreateWindowEx(0, PROGRESS_CLASS, NULL, WS_CHILD | WS_VISIBLE | PBS_SMOOTH, 20, 80, 265, 20, hwnd, (HMENU)8, hInstance, NULL);
	procent_wyk = CreateWindowEx(0, "STATIC", "0", WS_CHILD | WS_VISIBLE | SS_CENTERIMAGE | BS_PUSHBUTTON | SS_RIGHT, 292, 77, 25, 25, hwnd, (HMENU)9, hInstance, NULL);
	procent = CreateWindowEx(0, "STATIC", "%", WS_CHILD | WS_VISIBLE | SS_CENTERIMAGE | BS_PUSHBUTTON, 320, 77, 25, 25, hwnd, (HMENU)9, hInstance, NULL);
	czas_wyk = CreateWindowEx(0, "STATIC", "Czas od pocz�tku programu:", WS_CHILD | WS_VISIBLE | SS_CENTERIMAGE | BS_PUSHBUTTON | SS_RIGHT, 40, 105, 215, 20, hwnd, (HMENU)9, hInstance, NULL);
	czas_wyk_wynik = CreateWindowEx(0, "STATIC", "0", WS_CHILD | WS_VISIBLE | SS_CENTERIMAGE | BS_PUSHBUTTON | SS_RIGHT, 260, 105, 25, 20, hwnd, (HMENU)10, hInstance, NULL);
	procent = CreateWindowEx(0, "STATIC", "[s]", WS_CHILD | WS_VISIBLE | SS_CENTERIMAGE | BS_PUSHBUTTON, 290, 102, 25, 25, hwnd, (HMENU)9, hInstance, NULL);
	czas_poz = CreateWindowEx(0, "STATIC", "Czas do zako�czenia programu:", WS_CHILD | WS_VISIBLE | SS_CENTERIMAGE | BS_PUSHBUTTON | SS_RIGHT, 40, 125, 215, 20, hwnd, (HMENU)11, hInstance, NULL);
	czas_poz_wynik = CreateWindowEx(0, "STATIC", "0", WS_CHILD | WS_VISIBLE | SS_CENTERIMAGE | BS_PUSHBUTTON | SS_RIGHT, 260, 125, 25, 20, hwnd, (HMENU)12, hInstance, NULL);
	procent = CreateWindowEx(0, "STATIC", "[s]", WS_CHILD | WS_VISIBLE | SS_CENTERIMAGE | BS_PUSHBUTTON, 290, 122, 25, 25, hwnd, (HMENU)9, hInstance, NULL);
	Bufor2 = (LPSTR)GlobalAlloc(GPTR, 10);
	g_OldWndProc = (WNDPROC)SetWindowLong(hText, GWL_WNDPROC, (LONG)NewWndProc);
	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);
	while (GetMessage(&Komunikat, NULL, 0, 0))
	{
		TranslateMessage(&Komunikat);
		DispatchMessage(&Komunikat);
	}
	return Komunikat.wParam;
}

// OBS�UGA ZDARZE�
LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		if (stan != 0)
		{
			stop_program();
			GlobalFree(Bufor2);
		}
		PostQuitMessage(0);
		break;
	case WM_COMMAND:
		switch (wParam)
		{
		case 5:
			if (stan == 0)
			{
				start_program();
			}
			else if (stan == 2)
			{
				pocz = omp_get_wtime();
				ResumeThread(ahThread);
				ResumeThread(ahThread2);
				stan = 1;
			}
			break;
		case 6:
			if (stan == 1)
			{
				znacznik_czasu = omp_get_wtime();
				sum = sum + (znacznik_czasu - pocz);
				SetDlgItemText(hwnd, 10, itoa(sum, Bufor, 10));
				SetDlgItemText(hwnd, 12, itoa((100 - (int)(ilosc_wyk_perm * 100 / silnia(zawartosc_bufora)))*sum / (int)(ilosc_wyk_perm * 100 / silnia(zawartosc_bufora)), Bufor, 10));
				stan = 2;
				SuspendThread(ahThread);
				SuspendThread(ahThread2);
			}
			break;
		case 7:
			if (stan != 0)
			{
				znacznik_czasu = omp_get_wtime();
				sum = sum + (znacznik_czasu - pocz);
				SetDlgItemText(hwnd, 10, itoa(sum, Bufor, 10));
				SetDlgItemText(hwnd, 12, "0");
				stop_program();
				stan = 0;
				SetDlgItemText(hwnd, 9, "100");
				SendMessage(hProgressBar, PBM_SETPOS, (WPARAM)100, 0);
				MessageBox(NULL, "Przerwano przez u�ytkownika!", "OK", MB_ICONINFORMATION | MB_OK);
			}
			break;
		default:
			//MessageBox(hwnd, "Niewlasciwe polecenie", "ERROR", MB_ICONINFORMATION);
			break;
		}
		break;
	case WM_KEYDOWN:
	{
		switch ((int)wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case VK_RETURN:
			if (stan == 0)
			{
				start_program();
			}
			break;
		}
	}
	break;
	default:
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}
	return 0;
}

LRESULT CALLBACK NewWndProc(HWND hwnd, UINT mesg, WPARAM wParam, LPARAM lParam)
{
	switch (mesg)
	{
	case WM_KEYDOWN:
	{
		switch ((int)wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case VK_RETURN:
			if (stan == 0)
			{
				start_program();
			}
			break;
		}
	}
	break;
	}
	return CallWindowProc(g_OldWndProc, hwnd, mesg, wParam, lParam);
}